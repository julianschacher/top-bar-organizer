# Contributing

## Textwidth

For code of this repo in `.js` files, stick to a textwidth of 80.

## Commit Message Format

The commit message format is as follows:

```
tag: short description

Longer description here if necessary
```

The `tag` should be one of the following:

- `fix` - for a bug fix.
- `update` - for a backwards compatible enhancement.
- `feature` (formerly also `New`) - for a new feature.
- `breaking` - for a backwards-incompatible enhancement or feature.
- `perf` - for a code change that improves performance.
- `refactor` - for a code change that neither fixes a bug nor adds a feature (nor is `perf`).
- `build` - for changes that affect the build system or external dependencies.
- `test` - for adding or correcting tests.
- `docs` - for changes to documentation only.
- `other` - for anything that isn't covered by the tags above.

Those tags are an adapted version of the tags eslint (<https://eslint.org/docs/developer-guide/contributing/pull-requests#step-2-make-your-changes>) and of the tags Angular (<https://github.com/angular/angular/blob/master/CONTRIBUTING.md#type>) uses.
