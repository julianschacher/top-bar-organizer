Top Bar Organizer vX includes the following changes:

# Relevant and/or Breaking Changes

The following relevant and/or breaking changes of this version:

## Breaking Change

Description

## Relevant Change

Description

# Other Changes

- a change
- another change

# `git shortlog`

The git shortlog for this version:

```
git shortlog vX-1..vX
```
