# Top Bar Organizer

![Screenshot of GNOME Shell 43 with Top Bar Organizer v6 running and its preferences open. The GNOME Shell top bar items aren't all in their default location.](./res/Screenshot%20of%20GNOME%20Shell%2043%20with%20Top%20Bar%20Organizer%20v6%20and%20its%20preferences%20in%20light%20theme%202023-01-30.jpg)

Top Bar Organizer allows you to organize the items of the GNOME Shell top (menu)bar.

## Installation

The extension is available on the [GNOME Extensions website](https://extensions.gnome.org/extension/4356/top-bar-organizer/).  
Or you can also manually install a release from the [releases page](https://gitlab.gnome.org/june/top-bar-organizer/-/releases).

There's also a community-maintained [AUR package](https://aur.archlinux.org/packages/gnome-shell-extension-top-bar-organizer) available.
